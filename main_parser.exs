module main;


import exsdoc_parser::{ContainerTagKind, SimpleTagKind, AST, AstNode, Parser, createParser} from "./exsdoc_parser.exs" as {ContainerTagKind, SimpleTagKind, AST, AstNode, Parser,createParser};

def main(args (- string[])
{
    var parser = createParser(args[0]);

    parser.parse();
}
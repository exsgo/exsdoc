module doc_sample;


import System.{Math, Exception} as {Math, Exception};

/// ## Summary
/// An example function that adds *1* to the input.
def someFunction(value (- int)
-> int
{
    return value + 1;
}

/// ## Summary
/// An interface that returns the x value.
interface SomeInterface
{
    /// # Summary
    /// Returns the value of the `x` field.
    def getX() -> int;
}

/// ## Summary
/// A sample class that has a field, a __static__ method and a method.
/// -------------
/// This class is intended to be used alone.
class SomeClass : SomeInterface
{
    private let x (- int;

    /// ## Summary
    /// It returns a ***constant*** value.
    public static def someStaticMethod(value (- double) -> double
    {
        // コメント2
        return 10 / 2;
    }

    /// ## Summary
    /// It's a getter of the `x` field.
    /// Use case:
    /// ```
    /// def main()
    /// {
    ///     let inst = SomeClass{x: 1};
    ///     let x = inst.getX();
    /// }
    /// ```
    public def getX() -> int
    {
        return self.x;
    }
}

/// ## Summary
/// A sample enum.
/// - A
///     - *int*
/// - B
///     - __string__
///     - `int`
/// - C
///     - ***char***
export enum SomeEnum
{
    A(int),
    B(string, int),
    C(char)

    /// ## Summary
    /// Prints the value of the `self` object.
    /// This method prints the following variants:
    /// 1. *A*
    /// 1. __B__
    /// 1. `C`
    public def print()
    {
        match self {
            SomeEnum::A{value} => println("${value}");,
            SomeEnum::B{str, value} => println("${str}, ${value}");
            SomeEnum::C{value} => println("${value}");
        }
    }
}

/// ## Summary
/// It's an instance initializer.
/// Search in [google](https://www.google.com).
export def createInstance(value (- int)
{
    return SomeClass{x: value};
}

/// ## Summary
/// A sample module variable.
export let a = "abc";

/// # Summary
/// It's a little sine function.
/// | input | output |
/// |-------|--------|
/// |  *0*  |  *0*   |
/// |__pi/2__| __1__ |
/// |***pi***|***0***|
/// |`3pi/2`|  `-1`  |
/// | 2pi   |   0    |
///
/// # Exception
/// It throws when an unexpected value is given.
export def mySine(value (- double) -> double
{
    if value - 0.0 < 1.0e-8 {
        return 0.0;
    }else if value - Math.PI / 2.0 < 1.0e-8 {
        return 1.0;
    }else if value - Math.PI < 1.0e-8 {
        return 0.0;
    }else if value - 3.0 * Math.PI / 2.0 < 1.0e-8 {
        return -1.0;
    }else if value - 2 * Math.PI < 1.0e-8 {
        return 0.0;
    }else{
        throw Exception{message: "Unexpected value: ${value}"};
    }
}

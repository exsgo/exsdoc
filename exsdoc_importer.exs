module exsdoc_importer;


import System.Text.RegularExpressions.{Regex, RegexOptions, Match, GroupCollection} as {Regex, RegexOptions, Match, GroupCollection};
import System.IO.{StreamReader, Path} as {StreamReader, Path};
import System.Linq.Enumerable as Enumerable;
import System.Collections.Generic.IEnumerable<> as IEnumerable;
import exsdoc_parser::{AST, Parser, createParser} from "./exsdoc_parser.exs" as {AST, Parser, createParser};

let file_name_finder = Regex{pattern: r#"from\s+"([\w/\.]+)"\s+"#, options: RegexOptions.Compiled};
var imported_paths (- vector<string> = [...];

/// # Summary
/// The `Importer` class, which scans through Expresso source files for file names inside import statements 
/// and parses those files.
export class Importer
{
    private let current_file_name (- string;

    private def importImpl(reader (- StreamReader, asts (- vector<AST>, directoryPath (- string)
    {
        println("importImpl:");

        var line = reader.ReadLine();
        while line != null {
            if line.Contains("import ") {
                while !line.Contains("from ") && !line.Contains(";") {
                    line = reader.ReadLine();
                }

                if line.Contains("from ") {
                    let m = file_name_finder.Match(line);
                    let file_path = m.Groups[1].Value;

                    if file_path.Equals("std.exs") {
                        println("ignoring std.exs");
                        line = reader.ReadLine();
                        continue;
                    }

                    let index = file_path.LastIndexOf("/");
                    let file_name = (index == -1) ? file_path : file_path.Substring(index + 1);

                    if imported_paths.IndexOf(file_name) == -1 {
                        println("importing ${file_path} in ${self.current_file_name}...");

                        let importer = Importer{current_file_name: file_name};
                        let results = importer.execute(directoryPath, file_name);
                        asts.AddRange(results);
                        
                        imported_paths.Add(file_name);
                        println("finished importing ${file_path} in ${self.current_file_name}");
                    }
                }
            }
            
            if line.Contains("def ") || line.Contains("enum ") || line.Contains("class ") || line.Contains("interface ") {
                break;
            }

            line = reader.ReadLine();
        }
    }

    /// # Summary
    /// The public surface of the `Importer` class.
    public def execute(directoryPath (- string, fileName (- string) -> vector<AST>
    {
        let combined_path = Path.Combine(directoryPath, fileName);

        var main_parser = createParser(combined_path);
        var asts = [main_parser.parse(), ...];

        let main_file_reader (- StreamReader;
        try{
            main_file_reader = StreamReader{path: combined_path};
            self.importImpl(main_file_reader, asts, directoryPath);
        }
        finally{
            if main_file_reader != null {
                main_file_reader.Close();
            }
        }

        return asts;
    }
}

/// # Summary
/// Creates a new `Importer` class instance.
export def createImporter(fileName (- string)
{
    return Importer{current_file_name: fileName};
}
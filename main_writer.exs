module main;


import exsdoc_parser::{AST, Parser, createParser} from "./exsdoc_parser.exs" as {AST, Parser, createParser};
import exsdoc_writer::{Writer, createWriter} from "./exsdoc_writer.exs" as {Writer, createWriter};

def main(args (- string[])
{
    var parser = createParser(args[0]);
    let ast = parser.parse();

    let writer = createWriter(args[1]);
    writer.write(ast);

    println("documentation successfully generated!");
}
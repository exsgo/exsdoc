module exsdoc_writer;


import System.Exception as Exception;
import System.IO.{TextWriter, StreamWriter} as {TextWriter, StreamWriter};
import System.Text.RegularExpressions.{Regex, RegexOptions, Match, GroupCollection} as {Regex, RegexOptions, Match, GroupCollection};
import exsdoc_parser::{ContainerTagKind, SimpleTagKind, AstNode, AST} from "./exsdoc_parser.exs" as {ContainerTagKind, SimpleTagKind, AstNode, AST};
import std_expresso::Option<> from "std.exs" as Option;

let HEADER = r#"<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Test</title>
    <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link rel="stylesheet" href="./exsdoc.css" type="text/css">
</head>
<body>"#;

let FOOTER = r#"</body>
</html>"#;

/// # Summary
/// A `Regex` class instance that finds a module name from a string.
let module_name_finder = Regex{pattern: r"module\s+(\w+)", options: RegexOptions.Compiled};

/// # Summary
/// A `Regex` class instance that finds a class name from a string.
let class_name_finder = Regex{pattern: r"class\s+(\w+)", options: RegexOptions.Compiled};

/// # Summary
/// A `Regex` class instance that finds a function name from a string.
let function_name_finder = Regex{pattern: r"def\s+(\w+)", options: RegexOptions.Compiled};

/// # Summary
/// A `Regex` class instance that finds an enum name from a string.
let enum_name_finder = Regex{pattern: r"enum\s+(\w+)", options: RegexOptions.Compiled};

/// # Summary
/// A `Regex` class instance that finds an interface name from a string.
let interface_name_finder = Regex{pattern: r"interface\s+(\w+)", options: RegexOptions.Compiled};

/// # Summary
/// A `Regex` class instance that finds a variable name from a string.
let variable_name_finder = Regex{pattern: r"(?:let|var)\s+(\w+)", options: RegexOptions.Compiled};

/// # Summary
/// The writer class, which writes out the contents of ASTs to a html file.
export class Writer
{
    private var writer (- TextWriter;

    private def interpretSingleItem(ast (- AST) -> void
    {
        match ast {
            AST::ContainerTag{node, kind} => {
                if kind == ContainerTagKind::A {
                    self.writer.Write("<${kind.ToString()} href='");
                    let child = node.Child;
                    let href_node = child.unwrap().getNext();
                    self.interpretSingleItem(href_node.unwrap());
                    self.writer.Write("'>");

                    self.interpretSingleItem(child.unwrap());
                    self.writer.WriteLine("</${kind.ToString()}>");
                }else{
                    self.writer.Write("<${kind.ToString()}>");
                    self.interpretItems(node.Child);
                    self.writer.Write("</${kind.ToString()}>");
                }
            },
            AST::SimpleTag{node, kind} => {
                self.writer.WriteLine("<${kind.ToString()}>");
            },
            AST::Text{node, content} => {
                self.writer.Write("${content}");
            },
            _ => throw Exception{message: "Unexpected node found! `${ast.toString()}`"};
        }
    }

    private def interpretItems(option (- Option<AST>) -> void
    {
        var current = option;
        while current.isSome() {
            match current.unwrap() {
                AST::ContainerTag{node, kind} => {
                    self.interpretSingleItem(current.unwrap());

                    current = node.Next;
                },
                AST::SimpleTag{node, kind} => {
                    self.interpretSingleItem(current.unwrap());
                    current = node.Next;
                },
                AST::Text{node, content} => {
                    self.interpretSingleItem(current.unwrap());
                    
                    current = node.Next;
                },
                _ => throw Exception{message: "Unexpected node found! `${current.unwrap().toString()}`"};
            }
        }
    }

    private def interpretItemsAndTypeItems(ast (- Option<AST>) -> void
    {
        var current = ast;
        while current.isSome() {
            match current.unwrap() {
                AST::Field{node, field_definition} => {
                    self.interpretVariable(node, field_definition);
                    current = node.Next;
                },
                AST::Method{node, method_definition} => {
                    self.interpretFunction(node, method_definition);
                    current = node.Next;
                },
                AST::ContainerTag{node, _} => {
                    self.writer.WriteLine("");

                    self.interpretSingleItem(current.unwrap());
                    current = node.Next;

                    self.writer.WriteLine("");
                },
                AST::SimpleTag{node, _} => {
                    self.interpretSingleItem(current.unwrap());
                    current = node.Next;
                },
                AST::Text{node, _} => {
                    self.interpretSingleItem(current.unwrap());
                    current = node.Next;
                },
                _ => {
                    match ast {
                        Option::Some{value} => throw Exception{message: "Unexpected type item: ${value}"};,
                        Option::None{} => throw Exception{message: "Unexpected type item: Option::None()"};
                    }
                }
            }
        }
    }

    private def interpretClass(node (- AstNode, classDefinition (- string) -> void
    {
        println("interpreting a class: `${classDefinition}`");

        let m = class_name_finder.Match(classDefinition);
        self.writer.WriteLine(r#"<div id="${m.Groups[1].Value}" class="classes">"#);

        self.writer.WriteLine(r#"<h2 class="sectionTitles">${classDefinition}</h2>"#);
        var markdown = node.Child;
        self.interpretItemsAndTypeItems(markdown);

        self.writer.WriteLine("</div>");
    }

    private def interpretEnum(node (- AstNode, enumDefinition (- string) -> void
    {
        println("interpreting an enum: `${enumDefinition}`");

        let m = enum_name_finder.Match(enumDefinition);
        self.writer.WriteLine(r#"<div id="${m.Groups[1].Value}" class="enums">"#);

        self.writer.WriteLine(r#"<h2 class="sectionTitles">${enumDefinition}</h2>"#);
        let markdown = node.Child;
        self.interpretItemsAndTypeItems(markdown);

        self.writer.WriteLine("</div>");
    }

    private def interpretInterface(node (- AstNode, interfaceDefinition (- string) -> void
    {
        println("interpreting an interface: `${interfaceDefinition}`");

        let m = interface_name_finder.Match(interfaceDefinition);
        self.writer.WriteLine(r#"<div id="${m.Groups[1].Value}" class="interfaces">"#);

        self.writer.WriteLine(r#"<h2 class="sectionTitles">${interfaceDefinition}</h2>"#);
        let markdown = node.Child;
        self.interpretItemsAndTypeItems(markdown);

        self.writer.WriteLine("</div>");
    }

    private def interpretFunction(node (- AstNode, functionDefinition (- string) -> void
    {
        println("interpreting a function: `${functionDefinition}`");

        let m = function_name_finder.Match(functionDefinition);
        self.writer.WriteLine(r#"<div id="${m.Groups[1].Value}" class="functions">"#);

        self.writer.WriteLine(r#"<h2 class="sectionTitles">${functionDefinition}</h2>"#);
        self.interpretItems(node.Child);
        self.writer.WriteLine("");
        self.writer.WriteLine("</div>");
    }

    private def interpretVariable(node (- AstNode, variableDefinition (- string) -> void
    {
        println("interpreting a variable: `${variableDefinition}`");

        let m = variable_name_finder.Match(variableDefinition);
        self.writer.WriteLine(r#"<div id="${m.Groups[1].Value}" class="variable">"#);

        self.writer.WriteLine(r#"<h3 class="sectionTitles">${variableDefinition}</h3>"#);
        self.interpretItems(node.Child);
        self.writer.WriteLine("");
        self.writer.WriteLine("</div>");
    }

    private def navigation(ast (- AST)
    {
        println("generating the sidebar");

        self.writer.WriteLine(r#"<nav class="sidebar">"#);

        var current = Option::None<AST>{};

        match ast {
            AST::Module{node, module_definition} => {
                self.writer.WriteLine(r#"<p class="location">"#);
                self.writer.WriteLine("${module_definition}");
                self.writer.WriteLine("</p>");

                current = node.Child;
            },
            _ => throw Exception{message: "Unknown root node"};
        }

        self.writer.WriteLine("<ul>");
        while current.isSome() {
            match current.unwrap() {
                AST::Class{node, class_definition} => {
                    let m = class_name_finder.Match(class_definition);
                    self.writer.WriteLine(r#"<li><a href=" #${m.Groups[1].Value}">${m.Groups[1].Value}</a></li>"#);

                    current = node.Next;
                },
                AST::Enum{node, enum_definition} => {
                    let m = enum_name_finder.Match(enum_definition);
                    self.writer.WriteLine(r##"<li><a href=" #${m.Groups[1].Value}">${m.Groups[1].Value}</a></li>"##);

                    current = node.Next;
                },
                AST::Interface{node, interface_definition} => {
                    let m = interface_name_finder.Match(interface_definition);
                    self.writer.WriteLine(r#"<li><a href=" #${m.Groups[1].Value}">${m.Groups[1].Value}</a></li>"#);

                    current = node.Next;
                },
                AST::Method{node, func_definition} => {
                    let m = function_name_finder.Match(func_definition);
                    self.writer.WriteLine(r##"<li><a href=" #${m.Groups[1].Value}">${m.Groups[1].Value}</a></li>"##);

                    current = node.Next;
                },
                AST::Field{node, field_definition} => {
                    let m = variable_name_finder.Match(field_definition);
                    self.writer.WriteLine(r##"<li><a href=" #${m.Groups[1].Value}">${m.Groups[1].Value}</a></li>"##);

                    current = node.Next;
                },
                _ => throw Exception{message: "Unknown module child node."};
            }
        }

        self.writer.WriteLine("</ul>");
        self.writer.WriteLine("</nav>");
    }

    private def exsdoc(ast (- AST)
    {
        var current = Option::None<AST>{};
        self.navigation(ast);

        self.writer.WriteLine(r#"<section class="content">"#);

        match ast {
            AST::Module{node, module_definition} => {
                let m = module_name_finder.Match(module_definition);
                self.writer.WriteLine("<h1>module ${m.Groups[1].Value}:</h1>");
                println("generating contents for module ${m.Groups[1].Value}...");
                current = node.Child;
            },
            _ => throw Exception{message: "Unknown root node"};
        }

        while current.isSome() {
            match current.unwrap() {
                AST::Class{node, class_definition} => {
                    self.interpretClass(node, class_definition);
                    current = node.Next;
                },
                AST::Enum{node, enum_definition} => {
                    self.interpretEnum(node, enum_definition);
                    current = node.Next;
                },
                AST::Interface{node, interface_definition} => {
                    self.interpretInterface(node, interface_definition);
                    current = node.Next;
                },
                AST::Method{node, func_definition} => {
                    self.interpretFunction(node, func_definition);
                    current = node.Next;
                },
                AST::Field{node, variable_definition} => {
                    self.interpretVariable(node, variable_definition);
                    current = node.Next;
                },
                _ => throw Exception{message: "Unknown module child node!"};
            }
        }

        self.writer.WriteLine("</section>");
    }

    private def cleanup()
    {
        self.writer.WriteLine(FOOTER);
    }

    /// # Summary
    /// Actually writes out the contents to a html file.
    public def write(ast (- AST) -> void
    {
        try{
            self.writer.WriteLine(HEADER);

            self.exsdoc(ast);
            self.cleanup();
        }
        finally{
            self.writer.Flush();
            self.writer.Close();
        }
    }
}

/// # Summary
/// Creates a Writer instance.
export def createWriter(outFileName (- string)
{
    return Writer{writer: StreamWriter{path: outFileName} as TextWriter};
}

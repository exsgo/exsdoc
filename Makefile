DEST_DIR = lib/exsdoc
PREFIX = /usr
DEST = $(PREFIX)/$(DEST_DIR)
OUTPUT_DIR = bin
EXE = bin/exsdoc.exe
MAIN_SOURCE_FILE = main.exs
EXSDOC_LEXER = $(OUTPUT_DIR)/exsdoc_lexer.dll
EXSDOC_PARSER = $(OUTPUT_DIR)/exsdoc_parser.dll
EXSDOC_WRITER = $(OUTPUT_DIR)/exsdoc_writer.dll
EXSDOC_IMPORTER = $(OUTPUT_DIR)/exsdoc_importer.dll
STD_EXPRESSO = $(OUTPUT_DIR)/std_expresso.dll

all: exsdoc.exe

exsdoc.exe: $(MAIN_SOURCE_FILE)
	exsc $(MAIN_SOURCE_FILE) -o bin -e exsdoc

install: $(EXE)
	install -d $(DEST)
	install $(EXE) $(DEST)
	install $(EXSDOC_LEXER) $(DEST)
	install $(EXSDOC_PARSER) $(DEST)
	install $(EXSDOC_WRITER) $(DEST)
	install $(EXSDOC_IMPORTER) $(DEST)
	install $(STD_EXPRESSO) $(DEST)

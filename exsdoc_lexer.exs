module exsdoc_lexer;


import System.{Exception, Int64} as {Exception, Int64};
import System.IO.{Stream, StreamReader} as {Stream, StreamReader};
import std_expresso::Option<> from "std.exs" as Option;


/// # Summary
/// Represents a token.
export enum Token
{
    EOF(),
    EOL(),
    StartOfMarkdown(),
    StartOfSourceCode(),
    Symbol(int, char, int),
    Text(string)

    public def isEOF()
    {
        match self {
            Token::EOF{} => return true;,
            _ => return false;
        }
    }

    public def toString() -> string
    {
        match self {
            Token::EOF{} => return "EOF()";,
            Token::EOL{} => return "EOL()";,
            Token::StartOfMarkdown{} => return "StartOfMarkdown()";,
            Token::StartOfSourceCode{} => return "StartOfSourceCode()";,
            Token::Symbol{num, value, indent_level} => return "Symbol(${num}, ${value}, ${indent_level})";,
            Token::Text{value} => return "Text(${value})";
        }
    }
}

// can't replace with '\n' as int
// '\n'
let EOL = 13;
let EOF = -1;

/// # Summary
/// The exsdoc lexer, which scans through a file stream and returns tokens.
export class Lexer
{
    var reader (- StreamReader;
    var cur_token (- Option<Token>;
    var _pos (- Int64;                      // byte position in the current stream
    var ch (- int;                          // current input character
    var _char_pos (- int;                   // position by unicode character
    public var _Line (- int;                // line number
    public var _Col (- int;                 // column number
    var buf (- char[];                      // current token contents
    var buf_len (- int;                     // buffer length
    var cur_indent_level (- int;            // current indent level
    var seen_triple_slashes (- bool;        // whether this line starts with "///"
    var is_immediately_after_md (- bool;    // whether the current line is 1 line after markdown
    var seen_slash (- bool;                 // whether we have seen a slash
    var is_inside_type (- bool;             // whether we are inside type declarations
    var seen_triple_backticks (- bool;      // whether we have seen triple backticks
    var found_markdown (- bool;             // whether we've seen a markdown section
    var is_start_of_source_code (- bool;    // whether we have reached the beginning of source code

    private mutating def nextChar()
    {
        self._pos = self.reader.BaseStream.Position;
        if self.seen_slash && self.buf_len == 3 {
            println("set seen_slash to false");
            self.seen_slash = false;
        }
        self.ch = self.seen_slash ? self.buf[self.buf_len] as int : self.reader.Read();
        self._Col += 1;
        self._char_pos += 1;

        if self.ch == '\r' as int && self.reader.Peek() != EOL {
            self.ch = EOL;
        }

        if self.ch == EOL {
            self._Line += 1;
            self._Col = 0;
        }
    }

    private def peekChar()
    {
        var next_ch = self.reader.Peek();
        if next_ch == 10 && self.reader.Peek() != EOL {
            next_ch = EOL;
        }

        return next_ch;
    }

    private mutating def addChar()
    {
        if self.buf_len >= self.buf.Length {
            var new_buf = char[2 * self.buf.Length];
            array.Copy(self.buf as array, 0, new_buf as array, 0, self.buf.Length);
            self.buf = new_buf;
        }

        if self.ch != EOF {
            self.buf[self.buf_len] = self.ch as char;
            self.buf_len += 1;
            self.nextChar();
        }
    }

    private mutating def treatSequence(ch (- char)
    {
        var next_ch (- int;
        print("treatSequence:");
        while true {
            next_ch = self.ch;
            println("${self.buf_len}: '${next_ch as char}'");
            if next_ch == ch as int {
                self.addChar();
                println("matched");
            }else{
                println("not matched");
                break;
            }
        }
        println("");
    }

    private mutating def treatCodeBlock() -> Token
    {
        println("treatCodeBlock in Lexer:");
        var next_ch (- char;
        while true {
            next_ch = self.ch as char;

            match next_ch {
                '`' => {
                    self.addChar();
                    
                    var num_backticks = 1;
                    while true {
                        next_ch = self.ch as char;
                        if next_ch == '`' {
                            num_backticks += 1;
                            self.addChar();
                            
                            next_ch = self.ch as char;
                            if num_backticks == 3 && next_ch != '`' {
                                self.seen_triple_backticks = false;
                                self.cur_token = Option::Some<Token>{0: Token::Symbol{0: 3, 1: '`', 2: 0}};
                                break upto 2;
                            }else if num_backticks > 3 {
                                throw Exception{message: "Unknown sequence of `'s"};
                            } 
                        }else{
                            continue upto 2;
                        }
                    }
                },
                '\n' | '\r' => {
                    self.nextChar();
                    self.cur_token = Option::Some<Token>{0: Token::Text{0: string{value: self.buf, startIndex: 0, length: self.buf_len}}};
                    break;
                },
                _ => {
                    print("${next_ch}");
                    self.addChar();
                }
            }
        }

        println("");
        return self.cur_token.unwrap();
    }

    private mutating def clearBuffer()
    {
        array.Clear(self.buf as array, 0, self.buf.Length);
        self.buf_len = 0;
    }

    private mutating def skipCharacters()
    {
        println("Skip characters...");
        var next_ch (- char;
        var curly_brace_level = 0;
        var inspecting_import = false, seen_single_quote = false, seen_double_quote = false, seen_backslash = false, inside_line_comment = false;
        var inside_multiline_comment = false, newline_detected = false;
        while true {
            if self.ch == EOF {
                break;
            }

            next_ch = self.ch as char;
            if inside_line_comment || inside_multiline_comment {
                if inside_line_comment && (next_ch == '\n' || next_ch == '\r') {
                    println("comment ends");
                    inside_line_comment = false;
                    newline_detected = true;
                    self.addChar();
                }else if inside_multiline_comment && next_ch == '*' {
                    self.addChar();
                    next_ch = self.ch as char;
                    if next_ch == '/' {
                        println("comment ends*/");
                        inside_multiline_comment = false;
                    }else{
                        print("*${next_ch}");
                    }
                    self.addChar();
                }else{
                    print("${next_ch}");
                    self.addChar();
                }
            }else if next_ch == '/' {
                newline_detected = false;
                self.clearBuffer();
                self.addChar();
                print("${next_ch}");
                
                let second = self.ch as char;
                if second == '/' {
                    print("${second}");
                    self.addChar();
                }else if second == '*' {
                    inside_multiline_comment = true;
                    print("comment detected");
                    continue;
                }

                let third = self.ch as char;
                if third == '/' {
                    self.seen_slash = true;
                    println("${third}StartOfMarkdown detected");
                    return;
                }else{
                    inside_line_comment = true;
                    print("comment detected");
                }
                /*let num_chars_read = self.reader.Read(self.buf, 1, 2);
                if num_chars_read != 2 {
                    throw Exception{message: "There aren't 2 more characters in the stream."};
                }

                //println("buf: ${self.buf}");
                if self.buf[1] == '/' && self.buf[2] == '/' {
                    self.seen_slash = true;
                    println("StartOfMarkdown detected");
                    return;
                }else if self.buf[1] == '/' && self.buf[2] != '/' || self.buf[1] == '*' {
                    inside_comment = true;
                    println("comment detected");
                }else{
                    print("${next_ch}");
                    self.addChar();
                }*/
            }else if newline_detected && !inspecting_import && next_ch == 'i' {
                self.clearBuffer();
                newline_detected = false;

                self.addChar();
                print("i");
                let second = self.ch as char;
                if second != 'm' {
                    continue;
                }else{
                    print("m");
                    self.addChar();
                }
                let third = self.ch as char;
                if third != 'p' {
                    continue;
                }else{
                    print("p");
                    self.addChar();
                }
                let forth = self.ch as char;
                if forth != 'o' {
                    continue;
                }else{
                    print("o");
                    self.addChar();
                }
                let fifth = self.ch as char;
                if fifth != 'r' {
                    continue;
                }else{
                    print("r");
                    self.addChar();
                }
                let sixth = self.ch as char;
                if sixth != 't' {
                    continue;
                }else{
                    print("t");
                    self.addChar();
                }

                //let import_token = string{value: self.buf, startIndex: 0, length: self.buf_len};
                //if import_token.Equals("import") {
                inspecting_import = true;
                print("import (found)");
                /*}else{
                    if import_token.Contains("{") {
                        curly_brace_level += 1;
                        print("curly_brace_level = ${curly_brace_level}");
                    }
                    if import_token.Contains("}") {
                        curly_brace_level -= 1;
                        print("curly_brace_level = ${curly_brace_level}");
                    }

                    if double_quote_finder.IsMatch(import_token) {
                        seen_double_quote = !seen_double_quote;
                        print("seen_double_quote = ${seen_double_quote}");
                    }
                    // Expect single quotes in string literals
                    if !seen_double_quote && single_quote_finder.IsMatch(import_token) {
                        seen_single_quote = !seen_single_quote;
                        print("seen_single_quote = ${seen_single_quote}");
                    }
                    
                    print("${import_token}");
                }*/
            }else if inspecting_import && next_ch == ';' {
                inspecting_import = false;
                print(";");
                self.addChar();
            }else if next_ch == '}' {
                // found_markdown is needed because otherwise we would pause at a closing curly brace
                // when the method is not marked with markdown
                if self.is_inside_type && curly_brace_level == 0 {
                    print("${next_ch}");
                    self.is_inside_type = false;
                    break;
                }else if self.found_markdown && curly_brace_level == 1 {
                    print("${next_ch}");
                    self.found_markdown = false;
                    break;
                }else{
                    // to avoid decrementing the count when we see '}'
                    if !inspecting_import && !seen_single_quote {
                        curly_brace_level -= 1;
                        print("curly_brace_level = ${curly_brace_level}");
                    }
                    self.addChar();
                    print("${next_ch}");
                }
            }else if next_ch == '{' {
                // to avoid incrementing the count when we see '{'
                if !inspecting_import && !seen_single_quote {
                    curly_brace_level += 1;
                    print("curly_brace_level = ${curly_brace_level}");
                }
                self.addChar();
                print("${next_ch}");
            }else if next_ch == '\n' || next_ch == '\r' {
                print("${next_ch}");
                self.addChar();
                newline_detected = true;
            }else{
                if !seen_backslash && (next_ch == '\'' || next_ch == '"') {
                    // Expect single quotes in string literals
                    if !seen_double_quote && next_ch == '\'' {
                        seen_single_quote = !seen_single_quote;
                        print("seen_single_quote = ${seen_single_quote}");
                    }else if next_ch == '"' {
                        seen_double_quote = !seen_double_quote;
                        print("seen_double_quote = ${seen_double_quote}");
                    }
                }else if next_ch == '\\' {
                    seen_backslash = true;
                }else if seen_backslash {
                    seen_backslash = false;
                }else if next_ch != ' ' {
                    newline_detected = false;
                }
                print("${next_ch}");
                self.addChar();
            }
        }

        println("");
        self.clearBuffer();
    }

    private mutating def skipWhitespaces()
    {
        println("Skip whitespaces...");
        self.cur_indent_level = 0;
        var num_spaces = 0;
        var next_ch (- int;
        while true {
            next_ch = self.ch;
            if next_ch != ' ' as int {
                break;
            }else{
                num_spaces += 1;
                self.addChar();
                
                if num_spaces % 4 == 0 {
                    self.cur_indent_level += 1;
                }
            }
        }

        self.clearBuffer();
    }

    private mutating def treatText()
    {
        println("buf: ${self.buf}");
        var seen_equal_sign = false;
        while true {
            let next_int = self.ch;
            if next_int == EOF {
                break;
            }
            
            let next_ch = next_int as char;
            match next_ch {
                '*' | '_' | '<' | '`' | '|' | '[' | ']' | '(' | ')' if self.seen_triple_slashes => break;,
                '/' if !self.seen_triple_slashes => break;,
                '\r' | '\n' => {
                    if self.seen_triple_slashes {
                        self.seen_triple_slashes = false;
                        self.is_immediately_after_md = true;
                    }else{
                        self.nextChar();
                    }
                    break;
                },
                ';' | '{' => {
                    // don't eat { because we need this symbol to recognize the closing symbol
                    if next_ch == ';' {
                        self.addChar();
                        seen_equal_sign = false;
                    }else if seen_equal_sign && next_ch == '{' {
                        self.addChar();
                        continue;
                    }

                    if self.is_immediately_after_md {
                        self.is_immediately_after_md = false;
                    }

                    self.is_start_of_source_code = true;
                    break;
                },
                '=' => {
                    seen_equal_sign = true;
                    print("${next_ch}");
                    self.addChar();
                },
                _ => {
                    print("${next_ch}");
                    self.addChar();
                }
            }
        }

        println("");
        let buf_str = string{value: self.buf, startIndex: 0, length: self.buf_len};
        if buf_str.Contains("enum") || buf_str.Contains("class") || buf_str.Contains("interface") {
            self.is_inside_type = true;
        }
        self.cur_token = Option::Some<Token>{0: Token::Text{0: buf_str}};
    }

    private mutating def getNextToken() -> Token
    {
        self.clearBuffer();

        if self.is_start_of_source_code {
            println("Lexer: start of source code");
            self.cur_token = Option::Some<Token>{0: Token::StartOfSourceCode{}};
            self.is_start_of_source_code = false;
            return self.cur_token.unwrap();
        }

        if !self.seen_triple_slashes && !self.is_immediately_after_md {
            self.skipCharacters();
        }else{
            self.skipWhitespaces();
        }

        if self.seen_triple_backticks && self.ch as char != '/' {
            return self.treatCodeBlock();
        }

        if self.ch == EOF {
            self.cur_token = Option::Some<Token>{0: Token::EOF{}};
            println("EOF emitted");
            return self.cur_token.unwrap();
        }

        println("self.seen_triple_slashes: ${self.seen_triple_slashes} self.ch: '${self.ch as char}'");
        let next = self.ch as char;
        match next {
            next_symbol @ '*' | '-' | '`' | '#' | '~' | '>' | '_' if self.seen_triple_slashes => {
                println("case of signs in sequence: '${next_symbol}'");
                self.treatSequence(next_symbol);
                if next_symbol == '-' && self.buf_len == 1 && self.ch != ' ' as int {
                    self.treatText();
                }else{
                    self.cur_token = Option::Some<Token>{0: Token::Symbol{0: self.buf_len, 1: next, 2: self.cur_indent_level}};
                    if next == '`' && self.buf_len == 3 {
                        self.seen_triple_backticks = true;
                    }
                }
            },
            '[' | ']' | '(' | ')' | '|' | '+' | ':' | '\\' | '<' | '+' if self.seen_triple_slashes => {
                println("case of a sign: '${next}'");
                self.cur_token = Option::Some<Token>{0: Token::Symbol{0: 1, 1: next, 2: self.cur_indent_level}};
                self.addChar();
            },
            '}' => {
                println("case of closing curly brace: '${next}'");
                self.cur_token = Option::Some<Token>{0: Token::Symbol{0: 1, 1: next, 2: 0}};
                self.addChar();
            },
            '/' => {
                println("case of /");
                self.treatSequence('/');
                if self.buf_len == 3 {
                    self.seen_triple_slashes = true;
                    self.found_markdown = true;
                    self.cur_token = Option::Some<Token>{0: Token::StartOfMarkdown{}};
                }else{
                    self.cur_token = Option::Some<Token>{0: Token::Text{0: string{value: self.buf, startIndex: 0, length: self.buf_len}}};
                }
            },
            '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' => {
                println("numbers");
                let number = self.ch as char;
                self.addChar();

                let next_ch = self.ch as char;
                if next_ch == '.' {
                    self.addChar();
                    self.cur_token = Option::Some<Token>{0: Token::Symbol{0: 1, 1: number, 2: self.cur_indent_level}};
                }else{
                    self.treatText();
                }
            },
            '\r' | '\n' => {
                println("case of new line");
                self.addChar();
                if self.seen_triple_slashes {
                    self.seen_triple_slashes = false;
                    self.is_immediately_after_md = true;
                }
                
                self.cur_token = Option::Some<Token>{0: Token::EOL{}};
            },
            _ => {
                println("otherwise");
                self.treatText();
            }
        }

        return self.cur_token.unwrap();
    }

    /// # Summary
    /// Returns the next token.
    public mutating def peek() -> Token
    {
        println("lexer.peek");
        match self.cur_token {
            Option::Some{value} => return value;,
            Option::None{} => {
                println("Option::None{}");
                self.cur_token = Option::Some<Token>{0: self.getNextToken()};
                return self.cur_token.unwrap();
            }
        }
    }

    /// # Summary
    /// Returns the next token and comsumes it.
    public mutating def scan() -> Token
    {
        println("lexer.scan");
        match self.cur_token {
            Option::Some{value} => {
                println("Option::Some{{${value.toString()}}}");
                self.cur_token = Option::None<Token>{};
                return value;
            },
            Option::None{} => {
                println("Option::None{}");
                let token = self.getNextToken();
                self.cur_token = Option::None<Token>{};
                return token;
            }
        }
    }

    /// # Summary
    /// Reads the module statement.
    public def readModuleDefinition() -> string
    {
        if self.reader.BaseStream.Position != 0 as Int64 {
            throw Exception{message: "readModuleName should be called when the stream is at the very beginning."};
        }

        let line = self.reader.ReadLine();
        if !line.StartsWith("module") {
            throw Exception{message: "The stream doesn't start with the keyword 'module'."};
        }

        return line;
    }
}

/// # Summary
/// Creates a new lexer instance.
export def createLexer(fileName (- string) -> Lexer
{
    let reader = StreamReader{path: fileName};
    return Lexer{reader: reader, cur_token: Option::None<Token>{}, _pos: 0 as Int64, ch: 0, _char_pos: 0, _Line: 1, _Col: 0, buf: char[128], buf_len: 0,
    cur_indent_level: 0, seen_triple_slashes: false, is_immediately_after_md: false, seen_slash: false, is_inside_type: false, seen_triple_backticks: false,
    found_markdown: false, is_start_of_source_code: false};
}
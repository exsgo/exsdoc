module main;


import System.Exception as Exception;
import System.IO.{StreamWriter, Path} as {StreamWriter, Path};
import System.Text.RegularExpressions.{Regex, RegexOptions, Match, GroupCollection, Group} as {Regex, RegexOptions, Match, GroupCollection, Group};
import exsdoc_importer::{Importer, createImporter} from "./exsdoc_importer.exs" as {Importer, createImporter};
import exsdoc_parser::AST from "./exsdoc_parser.exs" as AST;
import exsdoc_writer::{Writer, createWriter} from "./exsdoc_writer.exs" as {Writer, createWriter};

let module_name_finder = Regex{pattern: r"module\s+(\w+)", options: RegexOptions.Compiled};
let CSS_CONTENT = r#"body{
    background-color: white;
}

.location {
    text-align: center;
    border: solid 1px black;
    background-color: white;
}

.sidebar{
    position: fixed;
    background-color: lightgray;
    width: 200px;
    left: 0;
    top: 0;
    height: 100vh;
    overflow: auto;
}

.sidebar ul {
    list-style: none;
}

.sidebar ul a {
    text-decoration: none;
    color: black;
    display: block;
    padding: .25rem .75rem;
}

.content{
    position: relative;
    margin-left: 230px;
}

.content h1{
    border: solid 1px black;
    border-radius: 5px;
    background-color: #f1f1f1;
    padding: .5em .75em;
    margin: 2em 0 1em;
}

.content h2:not([class="sectionTitles"]){
    border: solid 1px black;
    border-radius: 5px;
    background-color: #f1f1f1;
    padding: .5em .75em;
    margin: 2em 0 1em;
}

.content h3:not([class="sectionTitles"]){
    border: solid 1px black;
    border-radius: 5px;
    background-color: #f1f1f1;
    padding: .5em .75em;
    margin: 2em 0 1em;
}

.classes{
    border: inset 1px black;
    padding: 10px 0;
    margin: 10px 0;
}

.enums{
    border: outset 1px black;
    padding: 10px 0;
    margin: 10px 0;
}

.interfaces{
    border: inset 1px grey;
    padding: 10px 0;
    margin: 10px 0;
}

.functions{
    padding: 10px 0;
}
"#;

let HELP_MESSAGE = r#"exsdoc: the documentation generator for Expresso source files
Usage: exsdoc source_file_directory main_source_file_name(with extension) output_directory"#;

let VERSION = "0.3.1";

def main(args (- string[])
{
    let first_arg = args.Length > 0 ? args[0] : "";
    if args.Length == 0 || first_arg.Equals("--help") {
        println(HELP_MESSAGE);
        return;
    }else if first_arg.Equals("--version") {
        println("exsdoc version: ${VERSION}");
        return;
    }else if args.Length < 3 {
        println("exsdoc needs exactly 3 arguments!");
        return;
    }

    var importer = createImporter(args[1]);
    let asts = importer.execute(args[0], args[1]);

    for let ast in asts {
        match ast {
            AST::Module{_, module_definition} => {
                let m = module_name_finder.Match(module_definition);
                let module_name = m.Groups[1].Value;

                let writer = createWriter(Path.Combine(args[2], "${module_name}.html"));
                writer.write(ast);

                let css_writer (- StreamWriter;
                try{
                    css_writer = StreamWriter{path: Path.Combine(args[2], "exsdoc.css")};
                    css_writer.WriteLine(CSS_CONTENT);
                }
                finally{
                    if css_writer != null{
                        css_writer.Flush();
                        css_writer.Close();
                    }
                }
            },
            _ => throw Exception{message: "Unknown module item"};
        }
    }

    println("documentation successfully generated!");
}
module exsdoc_parser;


import std_expresso::Option<> from "std.exs" as Option;
import System.Exception as Exception;
import System.Text.StringBuilder as StringBuilder;
import exsdoc_lexer::{Token, Lexer, createLexer} from "./exsdoc_lexer.exs" as {Token, Lexer, createLexer};


/// # Summary
/// Represents the kind of a container tag.
export enum ContainerTagKind
{
    Em = 0,
    Strong,
    Code,
    Pre,
    A,
    Ul,
    Ol,
    Li,
    H1,
    H2,
    H3,
    H4,
    H5,
    H6,
    Del,
    Blockquote,
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Td

    public override def ToString()
    {
        match self {
            ContainerTagKind::Em => return "em";,
            ContainerTagKind::Strong => return "strong";,
            ContainerTagKind::Code => return "code";,
            ContainerTagKind::Pre => return "pre";,
            ContainerTagKind::A => return "a";,
            ContainerTagKind::Ul => return "ul";,
            ContainerTagKind::Ol => return "ol";,
            ContainerTagKind::Li => return "li";,
            ContainerTagKind::H1 => return "h1";,
            ContainerTagKind::H2 => return "h2";,
            ContainerTagKind::H3 => return "h3";,
            ContainerTagKind::H4 => return "h4";,
            ContainerTagKind::H5 => return "h5";,
            ContainerTagKind::H6 => return "h6";,
            ContainerTagKind::Del => return "del";,
            ContainerTagKind::Blockquote => return "blockquote";,
            ContainerTagKind::Table => return "table";,
            ContainerTagKind::Thead => return "thead";,
            ContainerTagKind::Tbody => return "tbody";,
            ContainerTagKind::Tr => return "tr";,
            ContainerTagKind::Th => return "th";,
            ContainerTagKind::Td => return "td";
        }
    }
}

enum TemporaryMdType
{
    None = 0,
    Class,
    Enum,
    Interface,
    Method,
    Field
}

/// # Summary
/// Represents the kind of a simple tag.
export enum SimpleTagKind
{
    Hr = 0,
    Br

    public override def ToString()
    {
        match self {
            SimpleTagKind::Hr => return "hr";,
            SimpleTagKind::Br => return "br";
        }
    }
}

/// # Summary
/// Represents either a html tag or a language construct.
export enum AST
{
    ContainerTag(AstNode, ContainerTagKind),
    SimpleTag(AstNode, SimpleTagKind),
    Text(AstNode, string),
    Method(AstNode, string),
    Class(AstNode, string),
    Interface(AstNode, string),
    Enum(AstNode, string),
    Field(AstNode, string),
    Module(AstNode, string),
    Undetermined(AstNode)   // This is only a temporal state

    /// # Summary
    /// Determines whether this node represents a ul tag.
    public def isUlTag() -> bool
    {
        match self {
            AST::ContainerTag{_, kind} => {
                if kind == ContainerTagKind::Ul {
                    return true;
                }else{
                    return false;
                }
            },
            _ => return false;
        }
    }

    /// # Summary
    /// Determines whether this node represents a table tag.
    public def isTableTag() -> bool
    {
        match self {
            AST::ContainerTag{_, kind} => {
                if kind == ContainerTagKind::Table {
                    return true;
                }else{
                    return false;
                }
            },
            _ => return false;
        }
    }

    /// # Summary
    /// Determines whether this node represents a tbody tag.
    public def isTbodyTag() -> bool
    {
        match self {
            AST::ContainerTag{_, kind} => {
                if kind == ContainerTagKind::Tbody {
                    return true;
                }else{
                    return false;
                }
            },
            _ => return false;
        }
    }

    /// # Summary
    /// Returns the next node.
    public def getNext() -> Option<AST>
    {
        match self {
            AST::ContainerTag{ast_node, _} => return ast_node.Next;,
            AST::SimpleTag{ast_node, _} => return ast_node.Next;,
            AST::Text{ast_node, _} => return ast_node.Next;,
            AST::Method{ast_node, _} => return ast_node.Next;,
            AST::Class{ast_node, _} => return ast_node.Next;,
            AST::Interface{ast_node, _} => return ast_node.Next;,
            AST::Enum{ast_node, _} => return ast_node.Next;,
            AST::Field{ast_node, _} => return ast_node.Next;,
            AST::Module{ast_node, _} => return ast_node.Next;,
            AST::Undetermined{ast_node} => return ast_node.Next;
        }
    }

    /// # Summary
    /// Sets the next node.
    public def setNext(next (- AST)
    {
        let option = Option::Some<AST>{0: next};
        match self {
            AST::ContainerTag{ast_node, _} => ast_node.Next = option;,
            AST::SimpleTag{ast_node, _} => ast_node.Next = option;,
            AST::Text{ast_node, _} => ast_node.Next = option;,
            AST::Method{ast_node, _} => ast_node.Next = option;,
            AST::Class{ast_node, _} => ast_node.Next = option;,
            AST::Interface{ast_node, _} => ast_node.Next = option;,
            AST::Enum{ast_node, _} => ast_node.Next = option;,
            AST::Field{ast_node, _} => ast_node.Next = option;,
            AST::Module{ast_node, _} => ast_node.Next = option;,
            AST::Undetermined{ast_node} => ast_node.Next = option;
        }
    }

    /// # Summary
    /// Returns the parent node.
    public def getParent() -> Option<AST>
    {
        match self {
            AST::ContainerTag{ast_node, _} => return ast_node.Parent;,
            AST::SimpleTag{ast_node, _} => return ast_node.Parent;,
            AST::Text{ast_node, _} => return ast_node.Parent;,
            AST::Method{ast_node, _} => return ast_node.Parent;,
            AST::Class{ast_node, _} => return ast_node.Parent;,
            AST::Interface{ast_node, _} => return ast_node.Parent;,
            AST::Enum{ast_node, _} => return ast_node.Parent;,
            AST::Field{ast_node, _} => return ast_node.Parent;,
            AST::Module{ast_node, _} => return ast_node.Parent;,
            AST::Undetermined{ast_node} => return ast_node.Parent;
        }
    }

    /// # Summary
    /// Sets the parent node.
    public def setParent(parent (- AST)
    {
        let option = Option::Some<AST>{0: parent};
        match self {
            AST::ContainerTag{ast_node, _} => ast_node.Parent = option;,
            AST::SimpleTag{ast_node, _} => ast_node.Parent = option;,
            AST::Text{ast_node, _} => ast_node.Parent = option;,
            AST::Method{ast_node, _} => ast_node.Parent = option;,
            AST::Class{ast_node, _} => ast_node.Parent = option;,
            AST::Interface{ast_node, _} => ast_node.Parent = option;,
            AST::Enum{ast_node, _} => ast_node.Parent = option;,
            AST::Field{ast_node, _} => ast_node.Parent = option;,
            AST::Module{ast_node, _} => ast_node.Parent = option;,
            AST::Undetermined{ast_node} => ast_node.Parent = option;
        }
    }

    private def setChildOrNext(current (- AstNode, ast (- AST)
    {
        match current.Child {
            Option::Some{child} => {
                var prev = current.Child;
                var option = child.getNext();

                while option.isSome() {
                    prev = option;
                    option = option.unwrap().getNext();
                }

                prev.unwrap().setNext(ast);
                ast.setParent(self);
            },
            Option::None{} => {
                current.Child = Option::Some<AST>{0: ast};
                ast.setParent(self);
            }
        }
    }

    /// # Summary
    /// Adds a child node.
    public def addChild(child (- AST)
    {
        match self {
            AST::ContainerTag{ast_node, _} => self.setChildOrNext(ast_node, child);,
            AST::SimpleTag{ast_node, _} => self.setChildOrNext(ast_node, child);,
            AST::Text{ast_node, _} => self.setChildOrNext(ast_node, child);,
            AST::Method{ast_node, _} => self.setChildOrNext(ast_node, child);,
            AST::Class{ast_node, _} => self.setChildOrNext(ast_node, child);,
            AST::Interface{ast_node, _} => self.setChildOrNext(ast_node, child);,
            AST::Enum{ast_node, _} => self.setChildOrNext(ast_node, child);,
            AST::Field{ast_node, _} => self.setChildOrNext(ast_node, child);,
            AST::Module{ast_node, _} => self.setChildOrNext(ast_node, child);,
            AST::Undetermined{ast_node} => self.setChildOrNext(ast_node, child);
        }
    }

    /// # Summary
    /// Returns the child node.
    public def getChild() -> Option<AST>
    {
        match self {
            AST::ContainerTag{ast_node, _} => return ast_node.Child;,
            AST::SimpleTag{ast_node, _} => return ast_node.Child;,
            AST::Text{ast_node, _} => return ast_node.Child;,
            AST::Method{ast_node, _} => return ast_node.Child;,
            AST::Class{ast_node, _} => return ast_node.Child;,
            AST::Interface{ast_node, _} => return ast_node.Child;,
            AST::Enum{ast_node, _} => return ast_node.Child;,
            AST::Field{ast_node, _} => return ast_node.Child;,
            AST::Module{ast_node, _} => return ast_node.Child;,
            AST::Undetermined{ast_node} => return ast_node.Child;
        }
    }

    /// # Summary
    /// Returns the string representation.
    public def toString() -> string
    {
        match self {
            AST::ContainerTag{_, kind} => return "AST::ContainerTag(_, ${kind.ToString()})";,
            AST::SimpleTag{_, kind} => return "AST::SimpleTag(_, ${kind.ToString()})";,
            AST::Text{_, content} => return "AST::Text(_, ${content})";,
            AST::Method{_, _} => return "AST::Method()";,
            AST::Class{_, _} => return "AST::Class()";,
            AST::Interface{_, _} => return "AST::Interface()";,
            AST::Enum{_, _} => return "AST::Enum()";,
            AST::Field{_, _} => return "AST::Field()";,
            AST::Module{_, _} => return "AST::Module()";,
            AST::Undetermined{_} => return "AST::Undetermined()";
        }
    }
}

/// # Summary
/// Holds other nodes.
export class AstNode
{
    public var Parent (- Option<AST>, Child (- Option<AST>, Next (- Option<AST>;
}

def createEmptyAstNode()
{
    return AstNode{Parent: Option::None<AST>{}, Child: Option::None<AST>{}, Next: Option::None<AST>{}};
}

def createContainerWithChild(kind (- ContainerTagKind, child (- AST)
{
    let node = createEmptyAstNode();
    var container = AST::ContainerTag{0: node, 1: kind};
    container.addChild(child);
    return container;
}

def createContainerWithText(kind (- ContainerTagKind, text (- string)
{
    let node = createEmptyAstNode();
    var container = AST::ContainerTag{0: node, 1: kind};
    
    let child_node = createEmptyAstNode();
    let child_text = AST::Text{0: child_node, 1: text};
    container.addChild(child_text);
    return container;
}

def createContainer(kind (- ContainerTagKind)
{
    let node = createEmptyAstNode();
    return AST::ContainerTag{0: node, 1: kind};
}

def createSimple(kind (- SimpleTagKind)
{
    var node = createEmptyAstNode();
    let container = AST::SimpleTag{0: node, 1: kind};
    return container;
}

def createText(content (- string)
{
    var node = createEmptyAstNode();
    let ast = AST::Text{0: node, 1: content};
    return ast;
}

/// # Summary
/// The parser, which is the public surface.
export class Parser
{
    private var lexer (- Lexer;
    private var topmost_ast (- Option<AST>;
    private var _is_quote (- bool;
    private var cur_ast_node (- Option<AstNode>;
    private var cur_ast (- Option<AST>;
    private var is_parsing_type (- bool;
    private var is_parsing_interface (- bool;
    private var cur_indent_level (- int;

    private def expectEOF() -> Option<Token>
    {
        println("expectEOF:");

        let token = self.lexer.peek();
        match token {
            Token::EOF{} => {
                self.lexer.scan();
                println("returning token");
                return Option::Some<Token>{0: token};
            },
            _ => return Option::None<Token>{};
        }
    }

    private def expectStartOfMarkdown() -> Option<Token>
    {
        println("expectStartOfMarkdown:");

        let token = self.lexer.peek();
        match token {
            Token::StartOfMarkdown{} => {
                println("StartOfMarkdown found");
                self.lexer.scan();
                return Option::Some<Token>{0: token};
            },
            _ => return Option::None<Token>{};
        }
    }

    private def skipToStartOfMarkdown() -> Option<Token>
    {
        println("skipToStartOfMarkdown:");

        var result = Option::None<Token>{};
        while true {
            let token = self.lexer.peek();
            match token {
                Token::StartOfMarkdown{} => {
                    println("StartOfMarkdown found");
                    self.lexer.scan();
                    result = Option::Some<Token>{0: token};
                    break;
                },
                Token::Symbol{_, symbol, _} => {
                    if symbol == '}' {
                        break;
                    }
                },
                Token::EOF{} => break;,
                _ => self.lexer.scan();
            }
        }

        return result;
    }

    private def expectNewLine() -> Option<Token>
    {
        let token = self.lexer.peek();
        match token {
            Token::EOL{} => {
                self.lexer.scan();
                return Option::Some<Token>{0: token};
            },
            _ => return Option::None<Token>{};
        }
    }

    private def expectSymbol(expected (- char) -> bool
    {
        let token = self.lexer.peek();
        match token {
            Token::Symbol{_, symbol, _} => {
                if symbol == expected {
                    self.lexer.scan();
                    return true;
                }else{
                    return false;
                }
            },
            _ => return false;
        }
    }

    private mutating def treatUnorderedList() -> Option<AST>
    {
        var ul_tag = Option::None<AST>{};
        var li_tag = Option::None<AST>{};
        var seen_hyphen = false;
        println("treatUnorderedList:");

        while true {
            let token2 = self.lexer.peek();
            match token2 {
                Token::Text{content} => {
                    if !seen_hyphen {
                        break;
                    }

                    li_tag = Option::Some<AST>{0: createContainerWithText(ContainerTagKind::Li, content)};
                    self.cur_ast.unwrap().addChild(li_tag.unwrap());
                    self.lexer.scan();
                },
                Token::Symbol{number, symbol, indent_level} => {
                    if symbol == '-' {
                        if !seen_hyphen {
                            seen_hyphen = true;
                        }else{
                            throw Exception{message: "Seen double -'s in a line"};
                        }

                        if self.cur_ast.isSome() && self.cur_ast.unwrap().isUlTag() {
                            if self.cur_indent_level < indent_level {
                                let new_ul = createContainer(ContainerTagKind::Ul);
                                self.cur_ast.unwrap().addChild(new_ul);
                                self.cur_ast = Option::Some<AST>{0: new_ul};
                                self.cur_indent_level += 1;
                            }else if self.cur_indent_level > indent_level {
                                while self.cur_indent_level > indent_level {
                                    self.cur_ast = self.cur_ast.unwrap().getParent();
                                    self.cur_indent_level -= 1;
                                }
                            }
                        }else{
                            ul_tag = Option::Some<AST>{0: createContainer(ContainerTagKind::Ul)};
                            self.cur_ast = ul_tag;
                            self.cur_indent_level = 0;
                        }

                        self.lexer.scan();
                    }else{
                        if !seen_hyphen {
                            throw Exception{message: "Unreachable"};
                        }

                        if !li_tag.isSome() {
                            li_tag = Option::Some<AST>{0: createContainer(ContainerTagKind::Li)};
                            self.cur_ast.unwrap().addChild(li_tag.unwrap());
                        }

                        self.lexer.scan();
                        match symbol {
                            '*' => {
                                let ast = self.treatAsterisk(number);
                                li_tag.unwrap().addChild(ast);
                            },
                            '`' => {
                                let ast = self.treatBacktick(number);
                                li_tag.unwrap().addChild(ast);
                            },
                            '~' => {
                                let ast = self.treatTilda(number);
                                li_tag.unwrap().addChild(ast);
                            },
                            '>' => {
                                let ast = self.treatGreterThan(number);
                                li_tag.unwrap().addChild(ast);
                            },
                            '_' => {
                                let ast = self.treatUnderscore(number);
                                li_tag.unwrap().addChild(ast);
                            },
                            _ => throw Exception{message: "Unexpected token on symbol in treatUnorderedList"};
                        }
                    }
                },
                Token::EOL{} => break;,
                _ => throw Exception{message: "Unexpected token in treatUnorderedList"};
            }
        }

        println("treatUnorderedList-end:");

        return ul_tag;
    }

    private mutating def treatOrderedList() -> Option<AST>
    {
        var ol_tag = Option::None<AST>{};
        var li_tag = Option::None<AST>{};
        var seen_number = false;
        println("treatOrderedList:");

        while true {
            let token2 = self.lexer.peek();
            match token2 {
                Token::Text{content} => {
                    if !seen_number {
                        break;
                    }

                    li_tag = Option::Some<AST>{0: createContainerWithText(ContainerTagKind::Li, content)};
                    self.cur_ast.unwrap().addChild(li_tag.unwrap());
                    self.lexer.scan();
                },
                Token::Symbol{number, symbol, indent_level} => {
                    if symbol == '0' || symbol == '1' || symbol == '2' || symbol == '3' || symbol == '4' || symbol == '5' || symbol == '6' || symbol == '7'
                    || symbol == '8' || symbol == '9' {
                        if !seen_number {
                            seen_number = true;
                        }else{
                            throw Exception{message: "Has seen double numbers in a line"};
                        }

                        if self.cur_ast.isSome() && self.cur_ast.unwrap().isUlTag() {
                            if self.cur_indent_level < indent_level {
                                let new_ol = createContainer(ContainerTagKind::Ol);
                                self.cur_ast.unwrap().addChild(new_ol);
                                self.cur_ast = Option::Some<AST>{0: new_ol};
                                self.cur_indent_level += 1;
                            }else if self.cur_indent_level > indent_level {
                                while self.cur_indent_level > indent_level {
                                    self.cur_ast = self.cur_ast.unwrap().getParent();
                                    self.cur_indent_level -= 1;
                                }
                            }
                        }else{
                            ol_tag = Option::Some<AST>{0: createContainer(ContainerTagKind::Ol)};
                            self.cur_ast = ol_tag;
                            self.cur_indent_level = 0;
                        }

                        self.lexer.scan();
                    }else{
                        if !seen_number {
                            throw Exception{message: "Unreachable"};
                        }

                        if !li_tag.isSome() {
                            li_tag = Option::Some<AST>{0: createContainer(ContainerTagKind::Li)};
                            self.cur_ast.unwrap().addChild(li_tag.unwrap());
                        }

                        self.lexer.scan();
                        match symbol {
                            '*' => {
                                let ast = self.treatAsterisk(number);
                                li_tag.unwrap().addChild(ast);
                            },
                            '`' => {
                                let ast = self.treatBacktick(number);
                                li_tag.unwrap().addChild(ast);
                            },
                            '~' => {
                                let ast = self.treatTilda(number);
                                li_tag.unwrap().addChild(ast);
                            },
                            '>' => {
                                let ast = self.treatGreterThan(number);
                                li_tag.unwrap().addChild(ast);
                            },
                            '_' => {
                                let ast = self.treatUnderscore(number);
                                li_tag.unwrap().addChild(ast);
                            },
                            _ => throw Exception{message: "Unexpected token on symbol in treatUnorderedList"};
                        }
                    }
                },
                Token::EOL{} => break;,
                _ => throw Exception{message: "Unexpected token in treatUnorderedList"};
            }
        }

        println("treatOrderedList-end:");

        return ol_tag;
    }

    private def treatCodeBlock() -> AST
    {
        var builder = StringBuilder{};
        while true {
            self.expectStartOfMarkdown();

            let token = self.lexer.scan();
            match token {
                Token::Text{text} => builder.AppendLine(text);,
                Token::Symbol{_, symbol, _} => {
                    if symbol == '`' {
                        break;
                    }else{
                        throw Exception{message: "Unexpected symbol '${symbol}'"};
                    }
                },
                _ => throw Exception{message: "Unexpected token type in treatCodeBlock"};
            }
        }

        return createContainerWithChild(ContainerTagKind::Pre, createContainerWithText(ContainerTagKind::Code, builder.ToString()));
    }

    private def treatAnchor() -> AST
    {
        println("treatAnchor");

        let token = self.lexer.scan();
        var a_tag = Option::None<AST>{};
        match token {
            Token::Text{content} => a_tag = Option::Some<AST>{0: createContainerWithText(ContainerTagKind::A, content)};,
            _ => throw Exception{message: "Expected a text token"};
        }

        if !self.expectSymbol(']') {
            throw Exception{message: "Expected ']'"};
        }

        if !self.expectSymbol('(') {
            throw Exception{message: "Expected '('"};
        }

        let token2 = self.lexer.scan();
        match token2 {
            Token::Text{href} => {
                let link_node = createText(href);
                a_tag.unwrap().addChild(link_node);
            },
            _ => throw Exception{message: "Expected a text token as href"};
        }

        if !self.expectSymbol(')') {
            throw Exception{message: "Expected ')'"};
        }

        return a_tag.unwrap();
    }

    private mutating def treatTable() -> Option<AST>
    {
        var table_tag = Option::None<AST>{};
        var thead_or_tbody_tag = Option::None<AST>{};
        var tr_tag = Option::None<AST>{};
        var th_or_td_tag = Option::None<AST>{};
        var seen_pipe = false;
        println("treatTable:");

        while true {
            let token = self.lexer.peek();
            match token {
                Token::Text{content} => {
                    if !seen_pipe {
                        break;
                    }

                    let text_node = createText(content);
                    th_or_td_tag.unwrap().addChild(text_node);

                    self.lexer.scan();
                },
                Token::Symbol{number, symbol, _} => {
                    if symbol == '|' {
                        println("| found");

                        if self.cur_ast.isSome() && (self.cur_ast.unwrap().isTableTag() || self.cur_ast.unwrap().isTbodyTag()) {
                            println("cur_ast is a table tag or a tbody tag");

                            if seen_pipe {
                                println("seen_pipe entered:");

                                tr_tag.unwrap().addChild(th_or_td_tag.unwrap());

                                println("seen_pipe exited:");
                            }else{
                                if self.cur_ast.isSome() && self.cur_ast.unwrap().isTbodyTag() {
                                    println("cur_ast is a tbody tag");
                                    
                                    thead_or_tbody_tag = self.cur_ast;
                                }else{
                                    println("cur_ast is a table tag");

                                    thead_or_tbody_tag = Option::Some<AST>{0: createContainer(ContainerTagKind::Tbody)};
                                    self.cur_ast.unwrap().addChild(thead_or_tbody_tag.unwrap());
                                    self.cur_ast = thead_or_tbody_tag;
                                }
                            }

                            if !tr_tag.isSome() {
                                tr_tag = Option::Some<AST>{0: createContainer(ContainerTagKind::Tr)};
                                thead_or_tbody_tag.unwrap().addChild(tr_tag.unwrap());
                            }
                            th_or_td_tag = Option::Some<AST>{0: createContainer(ContainerTagKind::Td)};

                            self.lexer.scan();
                        }else{
                            if seen_pipe {
                                tr_tag.unwrap().addChild(th_or_td_tag.unwrap());
                            }else{
                                thead_or_tbody_tag = Option::Some<AST>{0: createContainer(ContainerTagKind::Thead)};
                            }

                            if !tr_tag.isSome() {
                                tr_tag = Option::Some<AST>{0: createContainer(ContainerTagKind::Tr)};
                                thead_or_tbody_tag.unwrap().addChild(tr_tag.unwrap());
                            }
                            th_or_td_tag = Option::Some<AST>{0: createContainer(ContainerTagKind::Th)};

                            self.lexer.scan();

                            let token2 = self.lexer.peek();
                            // Defer the creation of a table tag here because otherwise we will enter the first if condition while we are still in a thead tag
                            match token2 {
                                Token::EOL{} => {
                                    table_tag = Option::Some<AST>{0: createContainerWithChild(ContainerTagKind::Table, thead_or_tbody_tag.unwrap())};
                                    self.cur_ast = table_tag;
                                },
                                _ => ;
                            }
                        }

                        if !seen_pipe {
                            seen_pipe = true;
                        }
                    }else{
                        if !seen_pipe {
                            throw Exception{message: "Unreachable"};
                        }

                        if !th_or_td_tag.isSome() {
                            let tag_kind = (self.cur_ast.isSome() && (self.cur_ast.unwrap().isTableTag() || self.cur_ast.unwrap().isTbodyTag())) ? ContainerTagKind::Td : ContainerTagKind::Th;
                            th_or_td_tag = Option::Some<AST>{0: createContainer(tag_kind)};
                        }

                        if symbol == '-' || symbol == ':' {
                            self.lexer.scan();
                            if number == 1 {
                                th_or_td_tag.unwrap().addChild(createText(string{c: symbol, count: 1}));
                            }
                            continue;
                        }

                        self.lexer.scan();
                        match symbol {
                            '*' => {
                                let ast = self.treatAsterisk(number);
                                th_or_td_tag.unwrap().addChild(ast);
                            },
                            '`' => {
                                let ast = self.treatBacktick(number);
                                th_or_td_tag.unwrap().addChild(ast);
                            },
                            '~' => {
                                let ast = self.treatTilda(number);
                                th_or_td_tag.unwrap().addChild(ast);
                            },
                            '>' => {
                                let ast = self.treatGreterThan(number);
                                th_or_td_tag.unwrap().addChild(ast);
                            },
                            '_' => {
                                let ast = self.treatUnderscore(number);
                                th_or_td_tag.unwrap().addChild(ast);
                            },
                            _ => throw Exception{message: "Unexpected token on symbol in treatTable"};
                        }
                    }
                },
                Token::EOL{} => break;,
                _ => throw Exception{message: "Unexpected token in treatTable"};
            }
        }

        println("treatTable-end:");

        return table_tag;
    }

    private def treatAsterisk(number (- int) -> AST
    {
        match number {
            1 => {
                let token = self.lexer.scan();
                match token {
                    Token::Text{content} => {
                        let next_token = self.lexer.scan();
                        match next_token {
                            Token::Symbol{number2, value, _} => {
                                if value != '*' {
                                    throw Exception{message: "Expected a closing *."};
                                }else if number != number2 {
                                    throw Exception{message: "Expected a pair of *'s."};
                                }else{
                                    return createContainerWithText(ContainerTagKind::Em, content);
                                }
                            },
                            _ => throw Exception{message: "Expected a symbol token."};
                        }
                    },
                    _ => throw Exception{message: "Expected a text token."};
                }
            },
            2 => {
                let token = self.lexer.scan();
                match token {
                    Token::Text{content} => {
                        let next_token = self.lexer.scan();
                        match next_token {
                            Token::Symbol{number2, value, _} => {
                                if value != '*' || number != number2 {
                                    throw Exception{message: "Expected a pair of *'s."};
                                }else{
                                    return createContainerWithText(ContainerTagKind::Strong, content);
                                }
                            },
                            _ => throw Exception{message: "Expected a text token."};
                        }
                    },
                    _ => throw Exception{message: "Expected a text token."};
                }
            },
            3 => {
                let token = self.lexer.scan();
                match token {
                    Token::EOL{} => return createSimple(SimpleTagKind::Hr);,
                    Token::Text{content} => {
                        let next_token = self.lexer.scan();
                        match next_token {
                            Token::Symbol{number2, value, _} => {
                                if value != '*' || number != number2 {
                                    throw Exception{message: "Expected a pair of *'s."};
                                }else{
                                    let child_ast = createContainerWithText(ContainerTagKind::Strong, content);
                                    return createContainerWithChild(ContainerTagKind::Em, child_ast);
                                }
                            },
                            _ => throw Exception{message: "Expected a text token."};
                        }
                    },
                    _ => throw Exception{message: "Expected a text token."};
                }
            },
            _ => {
                let token = self.lexer.scan();
                match token {
                    Token::EOL{} => return createSimple(SimpleTagKind::Hr);,
                    _ => throw Exception{message: "Expected a new line."};
                }
            }
        }
    }

    private mutating def treatHyphen(number (- int) -> Option<AST>
    {
        match number {
            1 => return self.treatUnorderedList();,
            2 => throw Exception{message: "Unknown number of -'s."};,
            _ => {
                self.lexer.scan();
                let token = self.lexer.peek();
                match token {
                    Token::EOL{} => return Option::Some<AST>{0: createSimple(SimpleTagKind::Hr)};,
                    _ => throw Exception{message: "Expected a new line."};
                }
            }
        }
    }

    private mutating def treatPlus(number (- int) -> Option<AST>
    {
        match number {
            1 => return self.treatUnorderedList();,
            _ => throw Exception{message: "Unexpected number of +'s"};
        }
    }

    private def treatBacktick(number (- int) -> AST
    {
        match number {
            1 => {
                let token = self.lexer.scan();
                match token {
                    Token::Text{content} => {
                        let token2 = self.lexer.scan();
                        match token2 {
                            Token::Symbol{number2, value, _} => {
                                if value != '`' {
                                    throw Exception{message: "Expected closing '`'."};
                                }else if number != number2 {
                                    throw Exception{message: "Expected a pair of `'s."};
                                }

                                let modified = string.Concat(content, " ");
                                return createContainerWithText(ContainerTagKind::Code, modified);
                            },
                            _ => throw Exception{message: "Expected a symbol token."};
                        }
                    },
                    _ => throw Exception{message: "Unexpected token in treatBacktick"};
                }
            },
            3 => return self.treatCodeBlock();,
            _ => throw Exception{message: "Backticks can only be used once or 3 times in sequence."};
        }
    }

    private def treatSharp(number (- int) -> AST
    {
        let token = self.lexer.scan();
        match token {
            Token::Text{content} => {
                match number {
                    1 => return createContainerWithText(ContainerTagKind::H1, content);,
                    2 => return createContainerWithText(ContainerTagKind::H2, content);,
                    3 => return createContainerWithText(ContainerTagKind::H3, content);,
                    4 => return createContainerWithText(ContainerTagKind::H4, content);,
                    5 => return createContainerWithText(ContainerTagKind::H5, content);,
                    6 => return createContainerWithText(ContainerTagKind::H6, content);,
                    _ => throw Exception{message: "Too many #'s."};
                }
            },
            _ => throw Exception{message: "Expected a text token."};
        }
    }

    private def treatTilda(number (- int) -> AST
    {
        match number {
            2 => {
                let token = self.lexer.scan();
                match token {
                    Token::Text{content} => {
                        let next_token = self.lexer.scan();
                        match next_token {
                            Token::Symbol{number2, value, _} => {
                                if value != '~' {
                                    throw Exception{message: "Expected a pair of ~'s."};
                                }else if number2 != number {
                                    throw Exception{message: "Expected a pair of double ~'s."};
                                }else{
                                    return createContainerWithText(ContainerTagKind::Del, content);
                                }
                            },
                            _ => throw Exception{message: "Expected a symbol token."};
                        }
                    },
                    _ => throw Exception{message: "Expected a text token."};
                }
            },
            3 => return self.treatCodeBlock();,
            _ => throw Exception{message: "Unknown number of ~'s."};
        }
    }

    private def treatGreterThan(_number (- int) -> AST
    {
        return createContainerWithText(ContainerTagKind::Blockquote, ""); 
        /*match number {
            1 => {
                if !self.is_quote {
                    self.is_quote = true;
                    self.writer.Write("<blockquote>");
                    let token = self.lexer.scan();
                    match token {
                        Token::Text{content} => {
                            self.writer.Write("${content}");
                            self.expectNewLine();
                        },
                        _ => throw Exception{message: "Expected a text token."};
                    }
                }
            },
            2 => {
                if !self.is_quote {
                    throw Exception{message: "Blockquote can't be shown up unless there is already a quote block."};
                }else{
                    self.writer.Write("<blockquote>");
                    let token = self.lexer.scan();
                    match token {
                        Token::Text{content} => {
                            self.writer.Write("${content}");
                            self.expectNewLine();
                        },
                        _ => throw Exception{message: "Expected a text token."};
                    }
                }
            }
        }*/
    }

    private def treatUnderscore(number (- int) -> AST
    {
        match number {
            1 => {
                let token = self.lexer.scan();
                match token {
                    Token::Text{content} => {
                        let next_token = self.lexer.scan();
                        match next_token {
                            Token::Symbol{number2, value, _} => {
                                if value != '_' || number != number2 {
                                    throw Exception{message: "Expected a pair of _'s."};
                                }else{
                                    return createContainerWithText(ContainerTagKind::Em, content);
                                }
                            },
                            _ => throw Exception{message: "Expected a symbol token."};
                        }
                    },
                    _ => throw Exception{message: "Expected a text token."};
                }
            },
            2 => {
                let token = self.lexer.scan();
                match token {
                    Token::Text{content} => {
                        let next_token = self.lexer.scan();
                        match next_token {
                            Token::Symbol{number2, value, _} => {
                                if value != '_' || number != number2 {
                                    throw Exception{message: "Expected a pair of _'s."};
                                }else{
                                    return createContainerWithText(ContainerTagKind::Strong, content);
                                }
                            },
                            _ => throw Exception{message: "Expected a text token."};
                        }
                    },
                    _ => throw Exception{message: "Expected a text token."};
                }
            },
            3 => {
                let token = self.lexer.scan();
                match token {
                    Token::EOL{} => return createSimple(SimpleTagKind::Hr);,
                    Token::Text{content} => {
                        let next_token = self.lexer.scan();
                        match next_token {
                            Token::Symbol{number2, value, _} => {
                                if value != '_' || number != number2 {
                                    throw Exception{message: "Expected a pair of _'s."};
                                }else{
                                    let child_ast = createContainerWithText(ContainerTagKind::Strong, content);
                                    return createContainerWithChild(ContainerTagKind::Em, child_ast);
                                }
                            },
                            _ => throw Exception{message: "Expected a text token."};
                        }
                    },
                    _ => throw Exception{message: "Expected a text token."};
                }
            },
            _ => {
                throw Exception{message: "Too many _'s found."};
            }
        }
    }

    private mutating def markdownContent(parent (- AST) -> void
    {
        println("markdownContent:");
        var eol_found = false;
        do{
            println("markdownContent-loop:");
            let token = self.lexer.peek();

            match token {
                Token::Symbol{number, value, _} => {
                    match value {
                        '*' => {
                            self.lexer.scan();
                            let ast = self.treatAsterisk(number);
                            parent.addChild(ast);
                        },
                        '-' => {
                            let ast = self.treatHyphen(number);
                            if ast.isSome() {
                                parent.addChild(ast.unwrap());
                            }
                        },
                        '+' => {
                            let ast = self.treatPlus(number);
                            if ast.isSome() {
                                parent.addChild(ast.unwrap());
                            }
                        },
                        '`' => {
                            self.lexer.scan();
                            let ast = self.treatBacktick(number);
                            parent.addChild(ast);
                        },
                        '#' => {
                            self.lexer.scan();
                            let ast = self.treatSharp(number);
                            parent.addChild(ast);
                        },
                        '~' => {
                            self.lexer.scan();
                            let ast = self.treatTilda(number);
                            parent.addChild(ast);
                        },
                        '>' => {
                            self.lexer.scan();
                            let ast = self.treatGreterThan(number);
                            parent.addChild(ast);
                        },
                        '_' => {
                            self.lexer.scan();
                            let ast = self.treatUnderscore(number);
                            parent.addChild(ast);
                        },
                        '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' => {
                            let ast = self.treatOrderedList();
                            if ast.isSome() {
                                parent.addChild(ast.unwrap());
                            }
                        },
                        '[' => {
                            self.lexer.scan();
                            let ast = self.treatAnchor();
                            parent.addChild(ast);
                        },
                        '|' => {
                            let ast = self.treatTable();
                            if ast.isSome() {
                                parent.addChild(ast.unwrap());
                            }
                        },
                        _ => throw Exception{message: "Unknown symbol found."};
                    }
                },
                Token::EOL{} => {
                    self.lexer.scan();
                    println("eol found");
                    eol_found = true;
                },
                Token::Text{content} => {
                    self.lexer.scan();
                    let ast = createText(content);
                    parent.addChild(ast);
                },
                _ => throw Exception{message: "Unexpected token: ${token.toString()}."};
            }
        }while !eol_found;

        println("markdownContent-end:");
    }

    private mutating def markdown()
    {
        println("markdown:");
        var token = self.skipToStartOfMarkdown();
        if token.isNone() {
            println("startOfMarkdown not found");
            return false;
        }

        self.cur_ast_node = Option::Some<AstNode>{0: createEmptyAstNode()};
        let parent_ast = AST::Undetermined{0: self.cur_ast_node.unwrap()};
        while token.isSome() {
            self.markdownContent(parent_ast);
            token = self.expectStartOfMarkdown();
        }
        self.cur_ast = Option::Some<AST>{0: parent_ast};
        println("markdown-end: cur_ast = ${self.cur_ast.unwrap().toString()}");

        return true;
    }

    private mutating def expectClosingCurlyBrace(isMethod (- bool)
    {
        println("expectClosingCurlyBrace:");

        let token = self.lexer.scan();
        println("${token.toString()}");
        match token {
            Token::Symbol{_, symbol, _} => {
                if symbol != '}' {
                    throw Exception{message: "Unexpected token '${symbol}'"};
                }else if !isMethod {
                    self.is_parsing_type = false;
                }
            },
            _ => throw Exception{message: "Unexpected token type"};
        }
    }

    private mutating def targetSourceCode()
    {
        println("targetSourceCode:");
        
        var str_builder = StringBuilder{};
        var cur_type = TemporaryMdType::None;
        while true {
            let token = self.lexer.scan();
            var type = Option::None<AST>{};
            match token {
                Token::EOL{} => {
                    str_builder.AppendLine();
                    println("EOL detected in targetSourceCode");
                },
                Token::Text{content} => {
                    println("Token::Text");
                    if str_builder.Length == 0 {
                        if content.Contains("class ") {
                            cur_type = TemporaryMdType::Class;
                        }else if content.Contains("def ") {
                            cur_type = TemporaryMdType::Method;
                        }else if content.Contains("enum ") {
                            cur_type = TemporaryMdType::Enum;
                        }else if content.Contains("interface ") {
                            cur_type = TemporaryMdType::Interface;
                        }else if content.Contains("let ") || content.Contains("var ") {
                            cur_type = TemporaryMdType::Field;
                        }else{
                            throw Exception{message: "Starts with an unknown keyword: ${content}"};
                        }
                    }
                    str_builder.Append(content);
                },
                Token::StartOfSourceCode{} => {
                    println("start of source code");
                    match cur_type {
                        TemporaryMdType::Class => {
                            self.cur_ast = Option::Some<AST>{0: AST::Class{0: self.cur_ast_node.unwrap(), 1: str_builder.ToString()}};
                            println("AST::Class() found");
                            type = self.cur_ast;
                            self.is_parsing_type = true;
                            
                            println("type = ${type.unwrap().toString()}");
                            match type.unwrap() {
                                AST::Class{node, _} => {
                                    println("type.Child = ${node.Child.unwrap().toString()}");
                                },
                                AST::Enum{node, _} => {
                                    println("type.Child = ${node.Child.unwrap().toString()}");
                                },
                                _ => throw Exception{message: "Unexpected type node"};
                            }
                        },
                        TemporaryMdType::Enum => {
                            self.cur_ast = Option::Some<AST>{0: AST::Enum{0: self.cur_ast_node.unwrap(), 1: str_builder.ToString()}};
                            println("AST::Enum() found");
                            type = self.cur_ast;
                            self.is_parsing_type = true;
                        },
                        TemporaryMdType::Interface => {
                            self.cur_ast = Option::Some<AST>{0: AST::Interface{0: self.cur_ast_node.unwrap(), 1: str_builder.ToString()}};
                            println("AST::Interface() found");
                            type = self.cur_ast;
                            self.is_parsing_interface = true;
                        },
                        TemporaryMdType::Method => {
                            self.cur_ast = Option::Some<AST>{0: AST::Method{0: self.cur_ast_node.unwrap(), 1: str_builder.ToString()}};
                            println("AST::Method() found");
                        },
                        TemporaryMdType::Field => {
                            self.cur_ast = Option::Some<AST>{0: AST::Field{0: self.cur_ast_node.unwrap(), 1: str_builder.ToString()}};
                            println("AST::Field() found");
                        },
                        _ => throw Exception{message: "Unknown state found."};
                    }

                    if !self.is_parsing_type && !self.is_parsing_interface {
                        println("add method to the module");
                        self.topmost_ast.unwrap().addChild(self.cur_ast.unwrap());
                    }

                    if cur_type == TemporaryMdType::Class || cur_type == TemporaryMdType::Enum || cur_type == TemporaryMdType::Interface {
                        println("parsing type children...");
                        while true {
                            if !self.docItem() {
                                break;
                            }else{
                                println("cur_ast = ${self.cur_ast.unwrap().toString()}");
                                type.unwrap().addChild(self.cur_ast.unwrap());
                            }
                        }

                        self.topmost_ast.unwrap().addChild(type.unwrap());
                        match type.unwrap() {
                            AST::Class{_, class_definition} => println("finishing parsing type children of ${class_definition}...");,
                            AST::Enum{_, enum_definition} => println("finishing parsing type children of ${enum_definition}...");,
                            AST::Interface{_, interface_definition} => {
                                println("finishing parsing type children of ${interface_definition}");
                                self.is_parsing_interface = false;
                            },
                            _ => throw Exception{message: "Unknown type kind"};
                        }
                    }

                    if cur_type != TemporaryMdType::Field && cur_type != TemporaryMdType::None && !self.is_parsing_interface {
                        self.expectClosingCurlyBrace(cur_type == TemporaryMdType::Method);
                    }
                    break;
                },
                _ => throw Exception{message: "Expected either a text token or a start of source code token."};
            }
        }
    }

    private mutating def docItem() -> bool
    {
        if !self.markdown() {
            return false;
        }
        self.targetSourceCode();
        return true;
    }

    private mutating def exsdoc()
    {
        println("exsdoc:");
        var cur_ast_node = createEmptyAstNode();
        let module_definition = self.lexer.readModuleDefinition();
        println("${module_definition}");
        self.topmost_ast = Option::Some<AST>{0: AST::Module{0: cur_ast_node, 1: module_definition}};

        while true {
            if !self.docItem() {
                break;
            }
        }
    }

    /// # Summary
    /// Parses the source file.
    public mutating def parse() -> AST
    {
        self.exsdoc();
        if self.expectEOF().isNone() {
            throw Exception{message: "Unexpected stream end"};
        }
        
        println("finished parsing");
        return self.topmost_ast.unwrap();
    }
}

export def createParser(fileName (- string) -> Parser
{
    return Parser{lexer: createLexer(fileName), topmost_ast: Option::None<AST>{}, _is_quote: false, cur_ast_node: Option::None<AstNode>{},
    cur_ast: Option::None<AST>{}, is_parsing_type: false, is_parsing_interface: false, cur_indent_level: 0};
}
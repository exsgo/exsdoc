module main;


import System.Exception as Exception;
import myoption::Option<> from "./myoption.exs" as Option;
import exsdoc_parser::{ContainerTagKind, SimpleTagKind, AST, AstNode, Parser, createParser} from "./exsdoc_parser.exs" as {ContainerTagKind, SimpleTagKind, AST, AstNode, Parser,createParser};

def outputConnectingNodes(node (- AstNode)
{
    let child = node.Child;
    let next = node.Next;
    println("Child = ${child.isSome() ? child.unwrap().toString() : child.toString()}, Next = ${next.isSome() ? next.unwrap().toString() : next.toString()}");
}

def outputNodes(ast (- Option<AST>) -> void
{
    var current = ast;
    while current.isSome() {
        match current.unwrap() {
            AST::ContainerTag{node, kind} => {
                println("${kind.ToString()}");
                outputConnectingNodes(node);
                outputNodes(node.Child);
                current = node.Next;
            },
            AST::SimpleTag{node, kind} => {
                println("${kind.ToString()}");
                outputConnectingNodes(node);
                current = node.Next;
            },
            AST::Text{node, content} => {
                println("${content}");
                outputConnectingNodes(node);
                current = node.Next;
            },
            AST::Method{node, name} => {
                println("function ${name}:");
                outputConnectingNodes(node);
                outputNodes(node.Child);
                current = node.Next;
            },
            AST::Class{node, name} => {
                println("class ${name}:");
                outputConnectingNodes(node);
                outputNodes(node.Child);
                current = node.Next;
            },
            AST::Enum{node, name} => {
                println("enum ${name}:");
                outputConnectingNodes(node);
                outputNodes(node.Child);
                current = node.Next;
            },
            AST::Field{node, name} => {
                println("field ${name}:");
                outputConnectingNodes(node);
                current = node.Next;
            },
            AST::Module{node, name} => {
                println("Unexpected Module node!");
            },
            AST::Undetermined{node} => {
                println("Unexpected Undetermined node!");
                current = node.Next;
            }
        }
    }
}

def outputAst(ast (- AST)
{
    match ast {
        AST::Module{node, name} => {
            println("module ${name}:");
            outputNodes(node.Child);
        },
        _ => throw Exception{message: "Unexpected node type on root!"};
    }
}

def main(args (- string[])
{
    let parser = createParser(args[0]);
    let ast = parser.parse();
    outputAst(ast);
}
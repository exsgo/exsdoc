module main;


import exsdoc_lexer::{Token, Lexer, createLexer} from "./exsdoc_lexer.exs" as {Token, Lexer, createLexer};

def printToken(token (- Token)
{
    match token {
        Token::EOF{} => println("EOF()");,
        Token::EOL{} => println("EOL()");,
        Token::StartOfMarkdown{} => println("Start of Markdown()");,
        Token::StartOfSourceCode{} => println("Start of source code");,
        Token::Symbol{number, value, indent_level} => println("Symbol(${number}: ${value}, indent_level: ${indent_level})");,
        Token::Text{value2} => println("Text(${value2})");
    }
}

def main(args (- string[])
{
    var lexer = createLexer(args[0]);

    var token (- Token;
    do{
        token = lexer.scan();
        printToken(token);
    }while !token.isEOF();
}